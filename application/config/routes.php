<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//for dashboard route
$route['dashboard'] = 'administrator_dashboard';



//for news route
$route['downloadables'] = 'administrator_downloadables';
$route['downloadables/add'] = 'administrator_downloadables/save_downloadables_page';
$route['downloadables/update/(:any)'] = 'administrator_downloadables/save_downloadables_page';
$route['downloadables/delete/(:any)'] = 'administrator_downloadables/delete_downloadables';
$route['downloadables/save_downloadables'] = 'administrator_downloadables/save_downloadables';


//for news route
$route['events'] = 'administrator_events';
$route['events/add'] = 'administrator_events/save_events_page';
$route['events/update/(:any)'] = 'administrator_events/save_events_page';
$route['events/delete/(:any)'] = 'administrator_events/delete_events';
$route['events/save_events'] = 'administrator_events/save_events';




//for news route
$route['news'] = 'administrator_news';
$route['news/add'] = 'administrator_news/save_news_page';
$route['news/update/(:any)'] = 'administrator_news/save_news_page';
$route['news/delete/(:any)'] = 'administrator_news/delete_news';
$route['news/save_news'] = 'administrator_news/save_news';


//for projects route
$route['projects'] = 'administrator_projects';
$route['projects/add'] = 'administrator_projects/save_project_page';
$route['projects/update/(:any)'] = 'administrator_projects/save_project_page';
$route['projects/delete/(:any)'] = 'administrator_projects/delete_project';
$route['projects/save_project'] = 'administrator_projects/save_project';