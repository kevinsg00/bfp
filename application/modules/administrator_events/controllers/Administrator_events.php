<?php
class Administrator_events extends MX_Controller {

	function __construct() {
        parent::__construct();
       $this->load->library("Curl");
    }
    public function index(){
        $item=$this->input->get();
        $data["list_events"]=($item)?$this->getAllEventsByTitle($item):$this->getAllEvents();
        $data['api_url']=$this->config->item("api_url");
        $data["title"] = "BFP: FIRE TITLE";
        $data["content"]= $this->load->view('administrator_events_view',$data,true);
        $this->load->view('layout/administrator_layout',$data);
    }
	public function save_events_page(){
		
      
        $data["title"] = "BFP: FIRE TITLE"; 
         $data['api_url']=$this->config->item("api_url");
        $data["additional_script"]=$this->load->view("additional_script", $data,true);
        $data["additional_css"]=$this->load->view("additional_css",$data,true);
        $data["content"]= $this->load->view('administrator_events_save_view',$data,true);
        $this->load->view('layout/administrator_layout',$data);
    }

    public function getAllEvents()
    {	
    	
    	$get_data = $this->curl->callAPI('GET',$this->config->item("api_url")."/events" , false);
		$response = json_decode($get_data, true);
		return $response;
    }
     public function getAllEventsByTitle($data)
    {   
        
        if($data['search']=="")
        {
                $get_data = $this->curl->callAPI('GET',$this->config->item("api_url")."/events" , false);
        }
        else
        {
               $get_data = $this->curl->callAPI('GET',$this->config->item("api_url")."/events/SearchTitle/".$data['search'] , false);
        }
        $response = json_decode($get_data, true);
        return $response;
    }
    public function save_events()
    {
        $data=array(
            'thread_id' =>$this->input->post("thread_id"),
            'title' =>$this->input->post("title"),
            'owner' => "Administrator",
            'description' =>$this->input->post("description"),
            'isPublished' =>($this->input->post("isPublished")=="on")? 1 : 0,
            'created_at' => $this->input->post("created_at"),
            'updated_at' => $this->input->post("updated_at")
        );
        
        if($this->input->post("thread_id")==0)
        {
              $this->curl->callAPI('POST',$this->config->item("api_url")."/events" ,json_encode($data));
        }
        else
        {
         $this->curl->callAPI('PUT',$this->config->item("api_url")."/threads/".$this->input->post("thread_id") ,json_encode($data));
        }
         $this->session->set_flashdata("message","You Have Successfully Save Events");
         redirect(base_url() ."events");
    }
    public function delete_events()
    {
       $this->curl->callAPI('DELETE',$this->config->item("api_url")."/threads/".$this->uri->segment(3) ,false);
       $this->session->set_flashdata("message","You Have Successfully Delete Events");
       redirect(base_url() ."events");
    }


}