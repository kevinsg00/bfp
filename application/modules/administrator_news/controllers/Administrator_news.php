<?php
class Administrator_news extends MX_Controller {

	function __construct() {
        parent::__construct();
       $this->load->library("Curl");
    }
    public function index(){
        $item=$this->input->get();
        $data["list_news"]=($item)?$this->getAllNewsByTitle($item):$this->getAllNews();
        $data['api_url']=$this->config->item("api_url");
        $data["title"] = "BFP: FIRE TITLE";
        $data["content"]= $this->load->view('administrator_news_view',$data,true);
        $this->load->view('layout/administrator_layout',$data);
    }
	public function save_news_page(){
		
      
        $data["title"] = "BFP: FIRE TITLE"; 
         $data['api_url']=$this->config->item("api_url");
        $data["additional_script"]=$this->load->view("additional_script", $data,true);
        $data["additional_css"]=$this->load->view("additional_css",$data,true);
        $data["content"]= $this->load->view('administrator_news_save_view',$data,true);
        $this->load->view('layout/administrator_layout',$data);
    }

    public function getAllNews()
    {	
    	
    	$get_data = $this->curl->callAPI('GET',$this->config->item("api_url")."/news" , false);
		$response = json_decode($get_data, true);
		return $response;
    }
     public function getAllNewsByTitle($data)
    {   
        if($data['search']=="")
        {
                $get_data = $this->curl->callAPI('GET',$this->config->item("api_url")."/news" , false);
        }
        else
        {
               $get_data = $this->curl->callAPI('GET',$this->config->item("api_url")."/news/SearchTitle/".$data['search'] , false);
        }
     
        $response = json_decode($get_data, true);
        return $response;
    }
    public function save_news()
    {
        $data=array(
            'thread_id' =>$this->input->post("thread_id"),
            'title' =>$this->input->post("title"),
            'owner' => "Administrator",
            'description' =>$this->input->post("description"),
            'isPublished' =>($this->input->post("isPublished")=="on")? 1 : 0,
            'created_at' => $this->input->post("created_at"),
            'updated_at' => $this->input->post("updated_at")
        );
        
        if($this->input->post("thread_id")==0)
        {
              $this->curl->callAPI('POST',$this->config->item("api_url")."/news" ,json_encode($data));
        }
        else
        {
         $this->curl->callAPI('PUT',$this->config->item("api_url")."/threads/".$this->input->post("thread_id") ,json_encode($data));
        }
         $this->session->set_flashdata("message","You Have Successfully Save News");
         redirect(base_url() ."news");
    }
    public function delete_news()
    {
       $this->curl->callAPI('DELETE',$this->config->item("api_url")."/threads/".$this->uri->segment(3) ,false);
       $this->session->set_flashdata("message","You Have Successfully Delete News");
       redirect(base_url() ."news");
    }


}