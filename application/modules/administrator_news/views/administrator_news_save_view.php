<?php
$response =null;
if($this->uri->segment(3)!=null)
{
  $get_data =$this->curl->callAPI('GET',$api_url."/threads/".$this->uri->segment(3),false);
 $response = json_decode($get_data, true);
}
 

?> 


<div class="row  pt-3">
	<div class="container">
	<div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Add News</h3>
       
      </div><!-- /.box-header -->
      <div class="box-body">
        
        <div class="container">
        <form method="POST" action="news/save_news">

            <div class="row">
                <input type="hidden" class="form-control" name="thread_id" placeholder="Title" value="<?= ($this->uri->segment(3)==null)?0:$this->uri->segment(3); ?>">
                 <input type="hidden" class="form-control" name="created_at" placeholder="Title" value="<?= ($response!=null)?$response['created_at']:date("Y-m-d h:i:s"); ?>">
                  <input type="hidden" class="form-control" name="updated_at" placeholder="Title" value="<?= ($response!=null)?$response['updated_at']:date("Y-m-d h:i:s"); ?>">
                <input type="text" class="form-control" name="title" placeholder="Title" value="<?= ($response!=null)?$response['title']:''; ?>">
            </div>
            <!-- Summernote text editor -->
            <div class="row pt-2">
              <textarea id="summernote" name="description"><?= ($response!=null)?$response['description']:''; ?></textarea>   
            </div>
             <div class="row pt-2">
            <div class="custom-control custom-switch">
              <input type="checkbox" class="custom-control-input" id="isPublished" name="isPublished" <?= ($response!=null&&$response['isPublished']==1)?'checked':''; ?>>
              <label class="custom-control-label" for="isPublished" style="font-size:18px;font-weight: bolder;">Published</label>
            </div>
          </div>
            <!-- Button for Save -->
             <div class="row pt-2 float-right">
                  <button type="submit" class="btn btn-success ">Save News</button>

                  <?php if($this->uri->segment(3)!=0): ?>
                  <a href="news/delete/<?=$this->uri->segment(3)?>" ><button class="btn btn-danger ml-2" type="button" >Delete News</button></a>
                   <?php endif ?>
            </div>
       </form>
        </div>




      </div><!-- /.box-body -->
    </div>

	</div>
</div>