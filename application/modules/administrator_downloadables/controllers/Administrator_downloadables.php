<?php
class Administrator_downloadables extends MX_Controller {

	function __construct() {
        parent::__construct();
       $this->load->library("Curl");
    }
    public function index(){
        $item=$this->input->get();
        $data["list_downloadables"]=($item)?$this->getAllNewsByTitle($item):$this->getAllNews();
        $data['api_url']=$this->config->item("api_url");
        $data["title"] = "BFP: FIRE TITLE";
        $data["content"]= $this->load->view('administrator_downloadables_view',$data,true);
        $this->load->view('layout/administrator_layout',$data);
    }
	public function save_downloadables_page(){
		
      
        $data["title"] = "BFP: FIRE TITLE"; 
        $data['maxSize']=$this->config->item("maxSize");
         $data['api_url']=$this->config->item("api_url");
        $data["additional_script"]=$this->load->view("additional_script", $data,true);
        $data["additional_css"]=$this->load->view("additional_css",$data,true);
        $data["content"]= $this->load->view('administrator_downloadables_save_view',$data,true);
        $this->load->view('layout/administrator_layout',$data);
    }

    public function getAllNews()
    {	
    	
    	$get_data = $this->curl->callAPI('GET',$this->config->item("api_url")."/downloadables" , false);
		$response = json_decode($get_data, true);
		return $response;
    }
     public function getAllNewsByTitle($data)
    {   
        if($data['search']=="")
        {
                $get_data = $this->curl->callAPI('GET',$this->config->item("api_url")."/downloadables" , false);
        }
        else
        {
               $get_data = $this->curl->callAPI('GET',$this->config->item("api_url")."/downloadables/SearchTitle/".$data['search'] , false);
        }
     
        $response = json_decode($get_data, true);
        return $response;
    }
    public function save_downloadables()
    {
        $output_dir = "uploads/downloadables/";
        $data=array(
            'id' =>$this->input->post("id"),
            'title' =>$this->input->post("nametitle"),
            'content' => $_FILES["fileupload"]["name"],
            'isPublished' =>($this->input->post("isPublished")=="on")? 1 : 0,
            'created_at' => $this->input->post("created_at"),
            'updated_at' => $this->input->post("updated_at")
        );
        if(isset($_FILES["fileupload"]))
        {
            $ret = array();
            $error =$_FILES["fileupload"]["error"];
            
            if(!is_array($_FILES["fileupload"]["name"])) //single file
            {
                $fileName = $_FILES["fileupload"]["name"];
                move_uploaded_file($_FILES["fileupload"]["tmp_name"],$output_dir.$fileName);
                $ret[]= $fileName;
                $this->save_details($data);
            }
            
           
         }
    }
     public function save_details($data)
    {
      
        if($data['id']==0)
        {
             $make_call=$this->curl->callAPI('POST',$this->config->item("api_url")."/downloadables/" ,json_encode($data));
            
        }
        else
        {
         $this->curl->callAPI('PUT',$this->config->item("api_url")."/downloadables/".$this->input->post("id") ,json_encode($data));
        }
         $this->session->set_flashdata("message","You Have Successfully Save Downloadables");
         echo json_encode($data);
    }
    public function delete_downloadables()
    {
       $this->curl->callAPI('DELETE',$this->config->item("api_url")."/Downloadables/".$this->uri->segment(3) ,false);
       $this->session->set_flashdata("message","You Have Successfully Delete News");
       redirect(base_url() ."downloadables");
    }


}