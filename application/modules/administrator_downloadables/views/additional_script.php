<link href="assets/uploadfile.css" rel="stylesheet">
<script type="text/javascript" src="assets/jquery/jquery.uploadfile.min.js"></script>

<script>
   $(document).ready(function()
{
   var uploader= $("#fileuploader").uploadFile({
    url:"downloadables/save_downloadables",
    autoSubmit:false,
    multiple:false,
    fileName:"fileupload",
    acceptFiles:"pdf",
   
   dynamicFormData: function()
    {
         var data =
        {
        "id":$("input[name=id]").val(),
        "title_name":$("input[name=title_name]").val(),
        "isPublished":$("input[name=isPublished]").val(),
        "created_at": $("input[name=created_at]").val(),
        "updated_at": $("input[name=updated_at]").val()
        }
        return data;
    },
    onError: function(files,status,errMsg,pd)
    {
         alert("error");
    },  
    });
  $(".btnSave").click(function(e)
     {
        
           uploader.startUpload();
     });
});
 

</script>
