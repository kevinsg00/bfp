<div class="row  pt-3">
	<div class="container">
	<div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Downloadables</h3>
       
      </div><!-- /.box-header -->
      <div class="box-body">
        
        <div class="container">
          <form method="GET">
            <div class="row">
              <div class="col-md-3">
                <a href="downloadables/add"><button class="btn btn-primary" type="button"><i class="fa fa-plus mr-2"></i>Add Downloadables</button></a>
              </div>
            
              <div class="col-md-7">
                <input type="text" class="form-control" name="search">
              </div>
              <div class="col-md-2">
                <button class="btn btn-primary"><i class="fa fa-search mr-2"></i>Search</button>
              </div>
             
            </div>
          <form>
            <div class="row">

              <div class="table-responsive">
                      <table class="table table-hover">
                          <thead>
                            <tr>
                              <th scope="col">title</th>
                             
                              <th scope="col">Created at</th>
                              <th scope="col">Updated at</th>
                              

                            </tr>
                          </thead>
                          <tbody>
                            <?php
                                foreach($list_downloadables as $response)
                                {    
                                    echo '<tr class="clickable-row" data-href="downloadables/update/'.$response['id'].'">';
                                    echo '<td>'.$response['title'].'</td>';         
                                    echo '<td>'.$response['created_at'].'</td>';
                                    echo '<td>'.$response['updated_at'].'</td>';
                                    echo '</a></tr>';
                                } 
                              ?>
                          </tbody>
                        </table>
               </div>
            </div>

        </div>




      </div><!-- /.box-body -->
    </div>

	</div>
</div>

