<!doctype html>
<html lang="en">
  <head>
    <base href="<?= base_url(); ?>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/style.css">
    <title>BFP</title>
  </head>
  <body style="font-size:14px">
  <div class="main-nav fixed-top ">
            <div class="container">
                    <nav class="navbar navbar-expand-lg  navbar-light bg-light">
                    <a class="navbar-brand" href="#">BFP</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">News</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Events</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Transparency</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Mapping</a>
                        </li>
                       
                        </ul>
                    </div>
                    </nav>
            </div>
    </div>
    
   <div class="container mt-5">
   <img class="img-fluid" src="images/bfp_header.png">
   </div>
   <div class="main-nav">
            <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav">
                        
                       
                        </ul>
                    </div>
                    <form class="form-inline">
                        <a href="#" style="margin:10px;">Login</a>
                    </form>
                    </nav>
            </div>
    </div>
    
    <div class="container">
        <?= $content ?>
    </div>
    <script src="assets/jquery/jquery.min.js"></script> 
    <script src="assets/bootstrap/js/bootstrap.min.js" ></script>
  </body>
</html>