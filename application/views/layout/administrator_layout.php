<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?=$title?></title>
  <base href="<?= base_url(); ?>">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="assets/template/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="assets/template/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" href="assets/template/css/skins/_all-skins.css">
  <link rel="stylesheet" href="assets/template/css/AdminLTE.css">
   <?php echo isset($additional_css) ? $additional_css:"" ?>  
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<link rel="icon" href="images/bfp-icon.ico">
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper" style="font-size:13px">

  <header class="main-header">
    <a href="index2.html" class="logo">
      <span class="logo-mini"><b>BFP</b></span>
      <span class="logo-lg"><b>BFP</b></span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
      
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="assets/template/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs">Alexander Pierce</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="assets/template/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  Alexander Pierce - Web Developer
                  <small>Member since Nov. 2012</small>
                </p>
              </li>
       
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?=base_url().'login';?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        
        </ul>
      </div>
    </nav>
  </header>
  
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="assets/template/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
        </div>
      </div>
      
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active">
          <a href="<?=base_url().'dashboard';?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
           
          </a>
       
        </li>
        <li >
          <a href="<?=base_url().'news';?>">
            <i class="fa fa-list"></i> <span>News</span>
           
          </a>
       
        </li>
         <li >
          <a href="<?=base_url().'projects';?>">
            <i class="fa fa-list"></i> <span>Projects</span>
           
          </a>
       
        </li>
        <li >
          <a href="<?=base_url().'events';?>">
            <i class="fa fa-calendar"></i> <span>Event</span>
           
          </a>
       
        </li>
        <li >
          <a href="<?=base_url().'dashboard';?>">
            <i class="fa fa-list"></i> <span>Tranparancy Seal</span>
           
          </a>
       
        </li>
        <li >
          <a href="<?=base_url().'dashboard';?>">
            <i class="fa fa-list"></i> <span>Bids and Awards</span>
           
          </a>
       
        </li>
        <li >
          <a href="<?=base_url().'downloadables';?>">
            <i class="fa fa-list"></i> <span>Downloadables</span>
           
          </a>
        </li>
          <li >
          <a href="<?=base_url().'dashboard';?>">
            <i class="fa fa-list"></i> <span>Advisories</span>
           
          </a>
       
        </li>
        <li >
          <a href="<?=base_url().'dashboard';?>">
            <i class="fa fa-list"></i> <span>Districts</span>
           
          </a>
       
        </li>
      
        <li >
          <a href="<?=base_url().'dashboard';?>">
            <i class="fa fa-list"></i> <span>Report</span>
           
          </a>
       
        </li>
        <li >
          <a href="<?=base_url().'dashboard';?>">
            <i class="fa fa-user"></i> <span>Account</span>
           
          </a>
       
        </li>
      </ul>
       <!-- Sidebar user panel -->
    </section>
  <!-- sidebar: style can be found in sidebar.less -->
  </aside>
  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

        <?php echo $content; ?>

  </div>


  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      
    </div>
   
  </footer>


  <div class="control-sidebar-bg"></div>
</div>
<script src="assets/jquery/jquery.min.js"></script> 
<script src="assets/jquery/popper.js" ></script>
<script src="assets/jquery/tooltip.js" ></script>
<script src="assets/bootstrap/js/bootstrap.js" ></script>


<script src="assets/template/js/adminlte.js" ></script>
 <?php echo isset($additional_script) ? $additional_script:"" ?>
</body>
</html>






  <?php if($this->session->flashdata("message") != ""): ?>
  <script type="text/javascript">
    alert("<?php echo $this->session->flashdata("message"); ?>");
  </script>
<?php endif ?>


<script type="text/javascript">
  jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
});
</script>